package com.datnx.demo3dtour;

/**
 * Created by NguyenDat on 10/10/15.
 */
public interface IBookmark {

    void onBookmark(ItemTest itemTest);
}
