package com.datnx.demo3dtour;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnOnline;
    private Button btnOffline;
    private Button btnListBuilding;
    private Button btnListBookmark;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnOnline = (Button) findViewById(R.id.btn_online);
        btnOffline = (Button) findViewById(R.id.btn_offline);
        btnListBuilding = (Button) findViewById(R.id.btn_list_building);
        btnListBookmark = (Button) findViewById(R.id.btn_list_bookmark);

        btnOnline.setOnClickListener(this);
        btnOffline.setOnClickListener(this);
        btnListBuilding.setOnClickListener(this);
        btnListBookmark.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v == btnOnline) {
            startActivity(new Intent(getApplicationContext(), ActivityWebView.class));
        } else if (v == btnOffline) {
            startActivity(new Intent(getApplicationContext(), ActivityOffline.class));
        } else if (v == btnListBuilding) {
            startActivity(new Intent(getApplicationContext(), ActivityListBuilding.class));
        } else {
            startActivity(new Intent(getApplicationContext(), ActivityListBookmark.class));
        }
    }
}
