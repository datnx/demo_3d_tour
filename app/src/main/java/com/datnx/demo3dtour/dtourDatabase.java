package com.datnx.demo3dtour;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created by NguyenDat on 10/10/15.
 */
public class dtourDatabase extends SQLiteOpenHelper {


    private static final String DB_NAME = "dtourDatabase";

    private static final String CREATE_TBL_BOOKMARK = "CREATE TABLE tbl_bookmark( bookmark_id INTEGER PRIMARY KEY, " +
            " building_id INTEGER, " +
            " bookmark INTEGER, " +
            " name TEXT)";

    public dtourDatabase(Context context) {
        super(context, DB_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TBL_BOOKMARK);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public boolean checkDB() {
        boolean checkDB = false;
        SQLiteDatabase database = this.getReadableDatabase();
        String query = "SELECT COUNT(*) FROM tbl_bookmark";
        Cursor cursor = database.rawQuery(query, null);
        if (cursor != null) {
            cursor.moveToFirst();
            if (cursor.getInt(0) == 0) {
                checkDB = true;
            }
        }
        cursor.close();
        return checkDB;
    }

    public ArrayList<ItemTest> checkIdBookmark() {
        ArrayList<ItemTest> itemTests = new ArrayList<ItemTest>();
        SQLiteDatabase database = this.getReadableDatabase();
        if (!checkDB()) {
            String query = "SELECT bookmark_id,building_id,bookmark,name FROM tbl_bookmark WHERE bookmark = " + 1;
            Cursor cursor = database.rawQuery(query, null);
            while (cursor.moveToNext()) {
                int id = cursor.getInt(0);
                int buildingId = cursor.getInt(1);
                int bookMark = cursor.getInt(2);
                String name = cursor.getString(3);
                itemTests.add(new ItemTest(-1, name, bookMark == 1 ? true : false, buildingId));
            }

            cursor.close();
            database.close();
        }
        return itemTests;
    }

    public void updateStatusBookmark(ItemTest itemTest, boolean isBookmark) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("name",itemTest.getTitle());
        values.put("building_id", itemTest.getId());
        values.put("bookmark", isBookmark ? 1 : 0);
        if (isBookmark) database.insert("tbl_bookmark", null, values) ;
        else database.update("tbl_bookmark", values, "building_id = ?",new String[]{String.valueOf(itemTest.getId())});
        database.close();
    }

    public ArrayList<ItemTest> getListBookmark() {
        ArrayList<ItemTest> itemTests = new ArrayList<ItemTest>();
        SQLiteDatabase database = this.getReadableDatabase();
        if (!checkDB()) {
            String query = "SELECT bookmark_id,building_id,bookmark,name FROM tbl_bookmark WHERE bookmark = " + 1;
            Cursor cursor = database.rawQuery(query, null);
            while (cursor.moveToNext()) {
                int id = cursor.getInt(0);
                int buildingId = cursor.getInt(1);
                int bookMark = cursor.getInt(2);
                String name = cursor.getString(3);
                itemTests.add(new ItemTest(-1, name, bookMark == 1 ? true : false, buildingId));
            }

            cursor.close();
            database.close();
        }
        return itemTests;
    }

}
