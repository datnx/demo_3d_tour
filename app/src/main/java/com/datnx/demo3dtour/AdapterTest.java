package com.datnx.demo3dtour;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by datnx on 10/10/15.
 */
public class AdapterTest extends ArrayAdapter<ItemTest> {

    private LayoutInflater layoutInflater;
    private IBookmark iBookmark;
    private IUnBookmark iUnBookmark;

    public AdapterTest(Context context,  List<ItemTest> objects) {
        super(context, -1, objects);
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ItemTest itemTest = getItem(position);
        ViewHolder viewHolder;
        if (null == convertView){
            viewHolder = new ViewHolder();
            convertView = layoutInflater.inflate(R.layout.layout_item_test, parent, false);
            viewHolder.txtTitle = (TextView) convertView.findViewById(R.id.txt_title);
            viewHolder.imgBookmark = (ImageView) convertView.findViewById(R.id.img_bookmark);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            viewHolder.txtTitle.setText("");
        }

        viewHolder.txtTitle.setText(itemTest.getTitle());
        if (itemTest.isBookmark()){
            viewHolder.imgBookmark.setImageResource(R.drawable.ic_bookmark);
        } else {
            viewHolder.imgBookmark.setImageResource(R.drawable.ic_unbookmark);
        }

        viewHolder.imgBookmark.setTag(position);
        viewHolder.imgBookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = (Integer) v.getTag();
                if (getItem(position).isBookmark()){
                    getItem(position).setIsBookmark(false);
                    iUnBookmark.onUnBookmark(getItem(position));
                } else {
                    getItem(position).setIsBookmark(true);
                    iBookmark.onBookmark(getItem(position));
                }
                notifyDataSetChanged();
            }
        });


        return convertView;
    }

    public void setiBookmark(IBookmark iBookmark) {
        this.iBookmark = iBookmark;
    }

    public void setiUnBookmark(IUnBookmark iUnBookmark) {
        this.iUnBookmark = iUnBookmark;
    }

    private class ViewHolder {
        private ImageView imgIcon;
        private TextView txtTitle;
        private ImageView imgBookmark;
    }

}
