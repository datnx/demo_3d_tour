package com.datnx.demo3dtour;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;



/**
 * Created by NguyenDat on 10/10/15.
 */
public class ActivityListBuilding extends Activity implements IBookmark, IUnBookmark, AdapterView.OnItemClickListener {

    private ListView listView;
    private AdapterTest adapterTest;
    private ArrayList<ItemTest> itemTests;
    private dtourDatabase dtourDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_building);
        listView = (ListView) findViewById(R.id.list_view);
        adapterTest = new AdapterTest(this, new ArrayList<ItemTest>());
        listView.setAdapter(adapterTest);
        itemTests = new ArrayList<ItemTest>();
        for (int i=0, n= 15; i<n ; i++){
            itemTests.add(new ItemTest(i,"Luxury building " + i, false));
        }
        checkData();
        adapterTest.setiBookmark(this);
        adapterTest.setiUnBookmark(this);
        listView.setOnItemClickListener(this);
        dtourDatabase = new dtourDatabase(getApplicationContext());
    }

    private void checkData() {

        TaskCheckIdBookmark taskCheckIdBookmark = new TaskCheckIdBookmark(){
            @Override
            protected void onPostExecute(ArrayList<ItemTest> data) {
                super.onPostExecute(data);
                for (int i=0, n=itemTests.size(); i<n; i++){
                    for (ItemTest itemTest : data){
                        if (itemTest.getId() == itemTests.get(i).getId()){
                            itemTests.get(i).setIsBookmark(true);
                            break;
                        }
                    }
                }
                updateListView(itemTests);
            }
        };

        taskCheckIdBookmark.execute(getApplicationContext());
    }

    private void updateListView(ArrayList<ItemTest> itemTests) {
        for (ItemTest data : itemTests){
            adapterTest.add(data);
        }
        adapterTest.notifyDataSetChanged();
    }

    @Override
    public void onBookmark(ItemTest itemTest) {
        dtourDatabase.updateStatusBookmark(itemTest, true);
    }

    @Override
    public void onUnBookmark(ItemTest itemTest) {
        dtourDatabase.updateStatusBookmark(itemTest, false);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        startActivity(new Intent(getApplicationContext(), ActivityWebView.class));
    }
}
