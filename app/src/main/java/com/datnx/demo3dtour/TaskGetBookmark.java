package com.datnx.demo3dtour;

import android.content.Context;
import android.os.AsyncTask;

import java.util.ArrayList;

/**
 * Created by NguyenDat on 10/10/15.
 */
public class TaskGetBookmark extends AsyncTask<Context, Void, ArrayList<ItemTest>> {

    @Override
    protected ArrayList<ItemTest> doInBackground(Context... params) {
        dtourDatabase dtourDatabase = new dtourDatabase(params[0]);
        ArrayList<ItemTest> itemTests = dtourDatabase.getListBookmark();
        return itemTests;
    }
}
