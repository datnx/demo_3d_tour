package com.datnx.demo3dtour;

import java.io.Serializable;

/**
 * Created by datnx on 10/10/15.
 */
public class ItemTest implements Serializable {

    private int icon;
    private String title;
    private boolean isBookmark;
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ItemTest(int icon, String title, boolean isBookmark, int id) {
        this.icon = icon;
        this.title = title;
        this.isBookmark = isBookmark;
        this.id = id;
    }

    public ItemTest() {
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isBookmark() {
        return isBookmark;
    }

    public void setIsBookmark(boolean isBookmark) {
        this.isBookmark = isBookmark;
    }

    public ItemTest(int id, String title, boolean isBookmark){
        this.id = id;
        this.title = title;
        this.isBookmark = isBookmark;
    }
}
