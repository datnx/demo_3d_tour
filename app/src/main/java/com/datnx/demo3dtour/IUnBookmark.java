package com.datnx.demo3dtour;

/**
 * Created by NguyenDat on 10/10/15.
 */
public interface IUnBookmark {
    void onUnBookmark(ItemTest itemTest);
}
