package com.datnx.demo3dtour;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by NguyenDat on 10/10/15.
 */
public class ActivityListBookmark extends Activity implements AdapterView.OnItemClickListener, IBookmark, IUnBookmark {

    private ListView listView;
    private AdapterTest2 adapterTest;
    private ArrayList<ItemTest> itemTests;
    private dtourDatabase dtourDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_building);
        listView = (ListView) findViewById(R.id.list_view);
        adapterTest = new AdapterTest2(this, new ArrayList<ItemTest>());
        listView.setAdapter(adapterTest);
        itemTests = new ArrayList<ItemTest>();

        checkData();
        adapterTest.setiBookmark(this);
        adapterTest.setiUnBookmark(this);
        listView.setOnItemClickListener(this);
        dtourDatabase = new dtourDatabase(getApplicationContext());
    }

    private void checkData() {

        TaskGetBookmark taskGetBookmark = new TaskGetBookmark(){
            @Override
            protected void onPostExecute(ArrayList<ItemTest> itemTests) {
                super.onPostExecute(itemTests);
                updateListView(itemTests);
            }
        };
        taskGetBookmark.execute(getApplicationContext());
    }

    private void updateListView(ArrayList<ItemTest> itemTests) {
        for (ItemTest data : itemTests){
            adapterTest.add(data);
        }
        adapterTest.notifyDataSetChanged();
    }

    @Override
    public void onBookmark(ItemTest itemTest) {
        dtourDatabase.updateStatusBookmark(itemTest, true);
    }

    @Override
    public void onUnBookmark(ItemTest itemTest) {
        dtourDatabase.updateStatusBookmark(itemTest, false);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        startActivity(new Intent(getApplicationContext(), ActivityWebView.class));
    }
}
